# Minima theme

Gtk3 theme with less space taking widgets, fork of Arc Gtk3 theme.

# Arc Theme

Arc is a flat theme with transparent elements for GTK 3, GTK 2 and GNOME Shell which supports GTK 3 and GTK 2 based desktop environments like GNOME, Unity, Pantheon, Xfce, MATE, Cinnamon (>=3.4), Budgie Desktop (10.4 for GTK+3.22) etc.

The NicoHood/arc-theme repository is a fork of the horst3180/arc-theme repository  which as been umaintained since March 2017.
Its aim is to continue the maintenance of arc-theme. The two maintainers are the Arch-Linux and Debian & Ubuntu packaging maintainers.

Original Arc theme is available here: https://github.com/NicoHood/arc-theme